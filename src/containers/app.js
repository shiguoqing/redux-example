import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux'
import { Router, Route, Link,browserHistory,hashHistory,IndexRoute,Redirect,IndexLink } from 'react-router'

import HomePage from '../pages/homePage/HomePage'
class App extends Component{
    render() {
        return (
            <Router history={hashHistory}>
                <Route path="/" component={HomePage}>
                    {
                        /* 当 url 为/时渲染 Dashboard */
                    //<IndexRoute component={Dashboard} />
                    //<Route path="about" component={About} />
                    //<Route path="inbox" component={Inbox}
                    //       onEnter={()=>console.log('我进入了inbox')}
                    //       onLeave={()=>console.log('我离开了inbox')}
                    //>
                    //</Route>
                    }
                </Route>
            </Router>
        )
    }
}

function mapStateToProps(state) {
    return {
        state1: state
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(TodoActions, dispatch)
    }
}


export default connect(
    mapStateToProps
)(App)

