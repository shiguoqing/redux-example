import * as types from '../constants/homeActionTypes'

export function getArticleList(articleList) {
    return { type: types.GET_ARTICLE_LIST, articleList }
}

export function getAdList(adList) {
    return { type: types.GET_AD_LIST, adList}
}

