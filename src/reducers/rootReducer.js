import { combineReducers } from 'redux'
import homeReducer from './homeReducer'
import detailReducer from './detailReducer'

const rootReducer = combineReducers({
    homeReducer,
    detailReducer
})


export default rootReducer