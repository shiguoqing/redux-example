//import { GET_AD_LIST,GET_ARTICLE_LIST} from '../constants/homeActionTypes'
import * as types from '../constants/homeActionTypes'

const initialState = {}

export default function homeReducer(state = initialState, action) {
    switch (action.type) {
        case types.GET_AD_LIST:
            return []

        case types.GET_ARTICLE_LIST:
            return []
        default:
            return state
    }
}
