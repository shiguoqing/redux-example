import * as types from '../constants/detailActionTypes'

export function getArticleDetail(articleId) {
    return { type: types.GET_ARTICLE_DETAIL, articleId }
}

export function submitComment(comment) {
    return { type: types.SUBMIT_COMMENT,comment}
}

