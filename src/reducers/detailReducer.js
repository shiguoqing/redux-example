import * as types from '../constants/detailActionTypes'

const initialState = {}

export default function homeReducer(state = initialState, action) {
    switch (action.type) {
        case types.GET_ARTICLE_DETAIL:
            return []

        case types.SUBMIT_COMMENT:
            return []
        default:
            return state
    }
}
