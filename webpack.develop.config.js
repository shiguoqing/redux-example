// 这是最基本的一个配置文件
// 编写配置文件，要有最基本的文件入口和输出文件配置信息等
// 里面还可以加loader和各种插件配置使用
var path = require('path');

module.exports = {
    entry:[
        'webpack/hot/dev-server',
        'webpack-dev-server/client?http://localhost:8080',
         path.resolve(__dirname,'src/index.js')
    ],
    output: {
        path: path.resolve(__dirname, 'build/js'),
        filename: 'bundle.js',
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/, // 用正则来匹配文件路径，这段意思是匹配 js 或者 jsx
                loader: 'babel',// 加载模块 "babel" 是 "babel-loader" 的缩写
                query: {
                    presets: ['es2015', 'react']
                }
            },
            {
                test: /\.css$/, // Only .css files
                loader: 'style!css' // Run both loaders
            },
            {
                test: /\.scss$/,
                loader: 'style!css!sass'
            },
            //{
            //    test: /\.(png|jpg)$/,
            //    loader: 'url?limit=25000'
            //},
            {
                test: /\.(png|jpeg|gif|jpg)$/,
                loader: 'file-loader?name=images/[name].[ext]'
            }


        ]
    },
    resolve: {
        //自动扩展文件后缀名，意味着我们require模块可以省略不写后缀名
        //注意一下, extensions 第一个是空字符串! 对应不需要后缀的情况.
        extensions: ['', '.js', '.json', '.scss', 'jsx'],

        //模块别名定义，方便后续直接引用别名，无须多写长长的地址
        alias: {//后续直接 require('AppStore') 即可
            //AppStore: 'js/stores/AppStores.js',
            //ActionType: 'js/actions/ActionType.js',
            //AppAction: 'js/actions/AppAction.js'
        }
    },
}
