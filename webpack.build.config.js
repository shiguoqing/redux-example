// 这是最基本的一个配置文件
// 编写配置文件，要有最基本的文件入口和输出文件配置信息等
// 里面还可以加loader和各种插件配置使用
var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    entry: {
        app: path.resolve(__dirname, 'src/js/app.js'),
        vendors: ['react', 'react-dom']
    },
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'bundle.js',
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/, // 用正则来匹配文件路径，这段意思是匹配 js 或者 jsx
                loader: 'babel',// 加载模块 "babel" 是 "babel-loader" 的缩写
                query: {
                    presets: ['es2015', 'react']
                }
            },
            {
                test: /\.css$/, // Only .css files
                loader: ExtractTextPlugin.extract("style-loader", "css-loader") // Run both loaders
            },
            {
                test: /\.scss$/,
                loader: 'style!css!sass'
            },
            //{
            //    test: /\.(png|jpg)$/,
            //    loader: 'url?limit=25000'
            //},
            {
                test: /\.(png|jpeg|gif|jpg)$/,
                loader: 'file-loader?name=images/[name].[ext]'
            }
        ]
    },
    resolve: {
        //自动扩展文件后缀名，意味着我们require模块可以省略不写后缀名
        //注意一下, extensions 第一个是空字符串! 对应不需要后缀的情况.
        extensions: ['', '.js', '.json', '.scss', 'jsx'],

        //模块别名定义，方便后续直接引用别名，无须多写长长的地址
        alias: {//后续直接 require('AppStore') 即可
            //AppStore: 'js/stores/AppStores.js',
            //ActionType: 'js/actions/ActionType.js',
            //AppAction: 'js/actions/AppAction.js'
        }
    },


    plugins: [
        new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.js'),
        new ExtractTextPlugin("styles.css")
    ]
}

